﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace _31._1___Auto
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += timer_Tick;
        }

        DispatcherTimer timer;
        Auto Volvo;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Volvo = new Auto();
            Volvo.TankChangeEvent += new Auto.AutoHandler(a_TankChangeEvent);
            Volvo.TankBijnaOpEvent += new Auto.AutoHandler(a_TankBijnaOpEvent);
            Volvo.TankOpEvent += new Auto.AutoHandler(a_TankOpEvent);
            Volvo.AutoGestartEvent += new Auto.AutoHandler(a_AutoGestartEvent);
            Volvo.AutoGestoptEvent += new Auto.AutoHandler(a_AutoGestoptEvent);
        }
        void timer_Tick(object sender, EventArgs e)
        {
            Volvo.VerminderAantalLiters();
        }

        //Buttons
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (Volvo.LitersInTank > 0)
            {
                Volvo.Snelheid = 75;
            }
            else
            {
                MessageBox.Show("U kan de auto niet opstarten, er is geen benzine aanwezig!");
            }

        }
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            Volvo.Snelheid = 0;
        }

        //Event Methods
        private void a_AutoGestoptEvent()
        {
            timer.Stop();
            lblDisplay.Content = Volvo.ToString();
        }
        private void a_TankOpEvent()
        {
            grdWindow.Background = new System.Windows.Media.SolidColorBrush(Colors.Red);
            Volvo.Snelheid = 0;
            MessageBox.Show("Tank is op! Auto wordt stilgelegd om ernstige te voorkomen", "Error Z0rgel00s", MessageBoxButton.OK, MessageBoxImage.Error);

        }

        private void a_TankBijnaOpEvent()
        {
            lblBericht.Content = "Piep piep! Tank bijna op." + Environment.NewLine + "Gelieve u naar het dichstbijzijnde tankstation te begeven.";
            grdWindow.Background = new System.Windows.Media.SolidColorBrush(Colors.Orange);
        }

        private void a_TankChangeEvent()
        {
            lblDisplay.Content = Volvo.ToString();
        }
        private void a_AutoGestartEvent()
        {
            timer.Start();
        }
    }
}
