﻿using System;

namespace _31._1___Auto
{
    internal class Auto
    {
        private int _litersInTank;
        private int _snelheid;

        public Auto(int litersInTank)
        {
            LitersInTank = litersInTank;
        }
        public Auto()
        {
            LitersInTank = 15;
            Snelheid = 0;

        }
        //Delegates
        public delegate void AutoHandler();
        //Events
        public event AutoHandler AutoGestartEvent;
        public event AutoHandler AutoGestoptEvent;
        public event AutoHandler TankBijnaOpEvent;
        public event AutoHandler TankChangeEvent;
        public event AutoHandler TankOpEvent;

        public int Snelheid
        {
            get { return _snelheid; }
            set
            {
                _snelheid = value;
                if (AutoGestartEvent != null || AutoGestoptEvent != null)
                {
                    if (value > 0)
                    {
                        AutoGestartEvent();
                    }
                    else
                    {
                        AutoGestoptEvent();
                    }
                }
            }
        }

        public int LitersInTank
        {
            get { return _litersInTank; }
            set
            {
                _litersInTank = value;

                if (TankChangeEvent != null)
                {
                    TankChangeEvent(); // Na verandering in property, gooien we de change event, we kijken naar if / else structuur voor de volgende events te bepalen.
                    if (TankBijnaOpEvent != null || TankOpEvent != null)
                    {
                        if (value <= 10 && value > 5)
                        {
                            TankBijnaOpEvent();
                        }
                        else if (value <= 5)
                        {
                            TankOpEvent();
                        }
                    }
                }
            }
        }
        //Methodes
        public override string ToString()
        {
            return Snelheid + "km/u" + Environment.NewLine + LitersInTank; ;
        }
        public void VerminderAantalLiters()
        {
            LitersInTank--;
        }


    }
}
